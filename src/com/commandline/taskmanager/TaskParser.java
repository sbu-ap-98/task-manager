package com.commandline.taskmanager;

import com.awsemn.deserializer.models.JSONArray;
import com.awsemn.deserializer.models.JSONObject;
import com.awsemn.deserializer.parser.JSONParser;
import com.commandline.taskmanager.model.BaseTask;
import com.commandline.taskmanager.model.CheckList;
import com.commandline.taskmanager.model.CheckListItem;
import com.commandline.taskmanager.model.TimedTask;
import com.commandline.taskmanager.util.DateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TaskParser {

    public static final String TASK_TYPE_NORMAL = "normal";
    public static final String TASK_TYPE_TIMED = "timed";
    public static final String TASK_TYPE_CHECKLIST = "checklist";

    private JSONParser parser = new JSONParser();

    public TaskParser(){

    }

    public List<BaseTask> parseTasks(String data){
        List<BaseTask> parsedTasks = new ArrayList<>();
        JSONArray array = parser.parse(data);
        for (int i=0;i<array.length();i++){
            parsedTasks.add(parseTask(array.getJSONObject(i)));
        }
        return parsedTasks;
    }

    private BaseTask parseTask(JSONObject jsonObject){
        String type = jsonObject.getString("type");
        switch (type){
            case TASK_TYPE_NORMAL:
                return parseNormalTask(jsonObject);
            case TASK_TYPE_TIMED:
                return parseTimedTask(jsonObject);
            case TASK_TYPE_CHECKLIST:
                return parseCheckList(jsonObject);
        }
        return null;
    }

    private BaseTask parseNormalTask(JSONObject jsonObject){
        BaseTask baseTask = new BaseTask(
                jsonObject.getString("title"),
                jsonObject.getString("description")
        );
        baseTask.setDone(jsonObject.getBoolean("done"));
        return baseTask;
    }

    private TimedTask parseTimedTask(JSONObject jsonObject){
        TimedTask timedTask = new TimedTask(
                jsonObject.getString("title"),
                jsonObject.getString("description")
        );
        try {
            timedTask.setDuoDate(new SimpleDateFormat(DateUtil.DATE_FORMAT).parse(jsonObject.getString("date")));
        } catch (ParseException e) {
            System.out.println("date has incorrect format");
        }
        timedTask.setDone(jsonObject.getBoolean("done"));
        return timedTask;
    }

    private CheckList parseCheckList(JSONObject jsonObject){
        CheckList checkList = new CheckList(
                jsonObject.getString("title"),
                jsonObject.getString("description")
        );
        checkList.setDone(jsonObject.getBoolean("done"));
        List<CheckListItem> items = parseCheckListItems(jsonObject.getJSONArray("items"));
        checkList.setCheckList(items);
        return checkList;
    }

    private List<CheckListItem> parseCheckListItems(JSONArray items) {
        List<CheckListItem> checkListItems = new ArrayList<>();
        for (int i=0;i<items.length();i++){
            checkListItems.add(parseCheckListItem(items.getJSONObject(i)));
        }
        return checkListItems;
    }

    private CheckListItem parseCheckListItem(JSONObject jsonObject) {
        return new CheckListItem(
                jsonObject.getString("name"),
                jsonObject.getBoolean("status")
        );
    }

    public JSONArray parseToJSON(List<BaseTask> tasks){
        JSONArray array = new JSONArray();
        for (BaseTask task : tasks){
            array.putJSONObject(parseTaskToJSON(task));
        }
        return array;
    }

    private JSONObject parseTaskToJSON(BaseTask task) {
        if (task instanceof CheckList){
            return parseCheckListToJSON((CheckList)task);
        } else if (task instanceof TimedTask){
            return parseTimedTaskToJSON((TimedTask) task);
        } else {
            return parseNormalTaskToJSON(task);
        }
    }

    private JSONObject parseNormalTaskToJSON(BaseTask task){
        JSONObject object = new JSONObject();
        object.put("title",task.getTitle());
        object.put("description",task.getDescription());
        object.put("done",task.isDone());
        object.put("type",TASK_TYPE_NORMAL);
        return object;
    }

    private JSONObject parseTimedTaskToJSON(TimedTask task){
        JSONObject object = new JSONObject();
        object.put("title",task.getTitle());
        object.put("description",task.getDescription());
        object.put("done",task.isDone());
        object.put("type",TASK_TYPE_TIMED);
        if (task.getDuoDate()!=null)
            object.put("date",new SimpleDateFormat(DateUtil.DATE_FORMAT).format(task.getDuoDate()));
        return object;
    }

    private JSONObject parseCheckListToJSON(CheckList task) {
        JSONObject object = new JSONObject();
        object.put("title",task.getTitle());
        object.put("description",task.getDescription());
        object.put("done",task.isDone());
        object.put("type",TASK_TYPE_CHECKLIST);

        JSONArray array = new JSONArray();
        for (CheckListItem item: task.getCheckList()){
            JSONObject itemObject = new JSONObject();
            itemObject.put("name",item.getName());
            itemObject.put("status",item.getStatus());
            array.putJSONObject(itemObject);
        }

        object.put("items",array);
        return object;
    }

}
