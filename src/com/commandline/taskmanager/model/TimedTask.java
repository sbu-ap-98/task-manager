package com.commandline.taskmanager.model;

import java.util.Date;

public class TimedTask extends BaseTask {

    private Date duoDate;

    public TimedTask(String title, String description) {
        super(title, description);
    }

    public TimedTask(String title,String description, Date date) {
        super(title,description);
        this.duoDate = date;
    }

    public Date getDuoDate() {
        return duoDate;
    }

    public void setDuoDate(Date duoDate) {
        this.duoDate = duoDate;
    }

    @Override
    public String toString() {
        return "TimedTask {" + "\n" +
                "title= " + getTitle() + "\n" +
                "description= " + getDescription() + "\n" +
                "duoDate= " + duoDate +
                '}';
    }
}
