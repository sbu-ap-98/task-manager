package com.commandline.taskmanager;

import com.commandline.taskmanager.io.FileManager;
import com.commandline.taskmanager.model.BaseTask;

import java.util.List;

public class DataStorage {

    private FileManager fileManager;

    private TaskParser parser = new TaskParser();

    public DataStorage(FileManager fileManager) {

        this.fileManager = fileManager;
    }

    public void saveTasks(List<BaseTask> tasks) {
        String str = parser.parseToJSON(tasks).toString();
        fileManager.saveFile("tasks.json", str);
    }

    public List<BaseTask> loadTasks() {
        String data = fileManager.readFile("tasks.json");
        if (data == null || data.isEmpty()) data = "[]";
        return parser.parseTasks(data);
    }
}
