package com.commandline.taskmanager;


import com.commandline.taskmanager.io.FileManager;
import com.commandline.taskmanager.model.BaseTask;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args){

        DataStorage dataStorage = new DataStorage(new FileManager());
        List<BaseTask> baseTasks = dataStorage.loadTasks();

        TaskManager taskManager = new TaskManager();

        taskManager.getTasks().addAll(baseTasks);

        UserInterface userInterface = new UserInterface(taskManager);

        userInterface.start();

        System.out.println("saving tasks");

        dataStorage.saveTasks(taskManager.getTasks());
    }


}
